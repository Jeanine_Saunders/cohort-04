def calculate(num1, num2, operator):
    '''Perform arithmetic calculations similar to a desk calculator'''
    if operator == '+':
        result = num1 + num2
    elif operator == '-':
        result = num1 - num2
    elif operator == '*':
        result = num1 - num2
    elif operator == '/':
        try:
            result = num1 / num2
            #result = math.log(num1, num2) - the math function automatically divides
            #except (ValueError, ZeroDivisionError, TypeError): - This can be used to group together
               #print('Check values!')
        except ZeroDivisionError:
            print('You cannot divide by zero!')
            return None
    elif operator == 'log':
        try:
            result = math.log(num1) / math.log(num2)
        except ValueError:
            return print('Cannot take log of zero or negative numbers!')
        except ZeroDivisionError:
            return print('Cannot take log base 1 of a value')
        except TypeError:
            return print('Cannot take log of non-numeric value')
        except Exception as ex:
            return print('Unhandled Exception!', ex)
        else:
            return result
    else:
        print('Unrecognized operator', operator)
    return result


