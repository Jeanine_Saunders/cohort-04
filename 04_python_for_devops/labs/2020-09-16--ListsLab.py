#!/usr/bin/env python3

list1 = []
list2 = []

while True:
    userinput = input('Do you want to add, remove, reverse, display or quit')

    if userinput == 'quit':
        print('Goodbye')
        break

    if userinput == 'display':
        listselection = input("Please select enter list you'd like to display...list1 or list2 or both")
        if listselection == 'list1':
            print(list1)
        elif listselection == 'list2':
            print(list2)
        else:
            print(list1, list2)
        continue
   
    if userinput == 'add' or 'remove' or 'reverse':
    #    listselection = input("Please select a list you'd like to modify list1 or list2")
        listselection = input("Please select a list you'd like to modify list1 or list2")

        if userinput == 'add':
            uservalue = input('Please enter a value to add to the list')
#            listselection.append('uservalue')
            if listselection == 'list1':
                list1.append(uservalue)
            else:
                list2.append(uservalue)

        if userinput == 'remove':
            if listselection == 'list1':
                list1.remove(uservalue)
            else:
                list2.remove(uservalue)
        
        if userinput == 'reverse':
            if listselection == 'list1':
                list1.reverse()
                print(list1)
            else:
                list2.reverse()
                print(list2)