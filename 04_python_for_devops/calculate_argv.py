import sys
from calculate import calculate

# Function call
print(sys.argv)
result = calculate(
    int(sys.argv[1]),
    int(sys.argv[2]),
    sys.argv[3])
#print('Executing script: ', sys.argv[0])
if result is None:
    print('Invalid calculation!')
else:
    print('The answer is', result)
